#!/usr/bin/env python3 
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

from bs4 import BeautifulSoup
import requests
import sys
import os

url = 'https://projecteuler.net/problem={}'
filename = 'problems/problem{}.py'

if len(sys.argv) != 2:
    sys.stderr.write("usage: download_problem NUM\n")
    sys.exit(1)

n = int(sys.argv[1])
url = url.format(n)
filename = filename.format(n)

if os.path.isfile(filename):
    raise ValueError("file already exists")

r = requests.get(url)
s = BeautifulSoup(r.text, 'html.parser')
f = s.find(id="content")
if not f:
    raise ValueError("content not found for pb {}".format(n))
f1 = f.find(name='h2')
if not f1:
    raise ValueError("h2 in content not found for pb {}".format(n))
title = f1.text
f2 = f.find(**{'name': "div", 'class': 'problem_content'})
if not f2:
    raise ValueError("problem content not found for pb {}".Format(n))
content = f2.text

output = open(filename, "w")
output.write("r\"\"\"\n")
output.write(title)
output.write("\n\n")
output.write(content)

output.write("\"\"\"\n\n")
output.write("def euler_problem_{}():\n    return -1".format(n))
output.close()
