Euler project in Python
=======================

This repository contains 20 exercises for programming in Python 3. The exercises are the
first 20 exercises from [Project Euler](https://projecteuler.net). If you are done with
these exercises, you can go on the website project euler and continue (there are more
than 500 problems!).

Each file  `problemX.py` in the repository `problems` (i.e.
`problems/problem1.py`, `problems/problem2.py`, `problems/problem3.py`, ...) is
intended to solve the problem `X` of the Project Euler. The file docstring contains
the complete description of the problem. For each file, you are asked to
program a function `euler_problem_X` that returns the answer to
the problem as an integer. A complete example is provided within the file
`problem1.py`.

You can check your answers by running the script `check_answers.py` as follows

    $ python3 check_answers.py

NOTE:

- Some exercises need additional data (namely problems 8, 11, 13 and 18). The relevant
  data is copied into text files. You can access the data using
  standard [Python input and output](https://docs.python.org/3/tutorial/inputoutput.html).

- The answers are stored only through their
  [SHA1 sum](https://en.wikipedia.org/wiki/Sha1sum) in the file `answers.json`.

This work is licensed under a Creative Commons Attribution-NonCommercial 4.0
International License (CC BY-NC-SA 4.0). You can either read the
[Creative Commons Deed](https://creativecommons.org/licenses/by-nc-sa/4.0/)
(Summary) or the [Legal Code](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
(Full licence).
