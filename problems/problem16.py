r"""
Power digit sum


2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2^1000?

Original problem at:

    https://projecteuler.net/problem=16
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

def euler_problem_16():
    return -1
