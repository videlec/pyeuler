r"""
10001st prime


By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that
the 6th prime is 13.

What is the 10 001st prime number?

Original problem at:

    https://projecteuler.net/problem=7
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

def euler_problem_7():
    return -1
