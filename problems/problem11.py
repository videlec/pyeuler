r"""
Largest product in a grid


Consider the 20×20 grid contained in the file "grid_problem11.txt". There is
a diagonal of length 4 made of the number 26, 63, 78, 14 whose product is
26 × 63 × 78 × 14 = 1788696.

What is the greatest product of four adjacent numbers in the same direction (up,
down, left, right, or diagonally) in the 20×20 grid?

Original problem at:

    https://projecteuler.net/problem=11
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

def euler_problem_11():
    return -1
