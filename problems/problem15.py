r"""
Lattice paths


Starting in the top left corner of a 2×2 grid, and only being able to move to
the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?

Original problem at:

    https://projecteuler.net/problem=15
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

def euler_problem_15():
    return -1
