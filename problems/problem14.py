r"""
Longest Collatz sequence


The following iterative sequence is defined for the set of positive integers:

n -> n/2 (n is even)
n -> 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains
10 terms. Although it has not been proved yet (Collatz Problem), it is thought
that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

Original problem at:

    https://projecteuler.net/problem=14
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

def euler_problem_14():
    return -1
