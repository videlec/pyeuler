#!/usr/bin/env python3
r"""
This file is a small script that checks the answer of the exercises.
"""
################################################################################
# This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 #
# International License (CC BY-NC-SA 4.0). You can either read the             #
# Creative Commons Deed at                                                     #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/                          #
#                                                                              #
# or the Legal Code at                                                         #
#                                                                              #
#  https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode                 #
################################################################################

import datetime
import hashlib
import json
import os, os.path
import pkgutil
import re
import signal
import sys

f = open("answers.json")
values = json.load(f)
f.close()

sys.path.append(os.path.abspath('problems'))
os.chdir('problems/')
name = re.compile("problem(\d+)\.py")

ERROR = 0
TIMEOUT = 1
WRONG_ANSWER = 2
GOOD_SLOW_ANSWER = 3
GOOD_FAST_ANSWER = 4
ans_types = range(5)

def alarm_handler(signum, frame):
    print('prout que prout')
    raise TimeoutError("the function is too long to answer (> 5secs)")

signal.signal(signal.SIGALRM, alarm_handler)

def check_answer(i):
    r"""
    Check the solution of problem ``i`` and return one of ``ERROR``,
    ``WRONG_ANSWER``, ``GOOD_SLOW_ANSWER`` or ``GOOD_FAST_ANSWER``.
    """
    pb = "problem%d" % i
    filename = "problem%d.py" % i
    print(pb)

    # check the existence of the file
    if not os.path.isfile(filename):
        print("{}.py does not exists".format(filename))
        return ERROR

    # load the file as a module
    # and compute time

    signal.alarm(10)
    try:
        t = datetime.datetime.now()
        loader = pkgutil.get_loader(pb)
        if loader is None:
            raise ValueError("loader failed for pb={} filename={}".format(pb, filename))
        try:
            mod = loader.load_module()
        except (SyntaxError, ValueError):
            print("problem while loading {}.py".format(pb))
            return ERROR

        try:
            f = getattr(mod, "euler_problem_%d" % i)
        except AttributeError:
            print("there should be a function euler_problem_{} in {}.py".format(i, pb))
            return ERROR

        ans = f()
        signal.alarm(0)
        t = (datetime.datetime.now() - t).total_seconds()
    except TimeoutError:
        print('timeout')
        return TIMEOUT

    print("computation went fine in {} seconds".format(t))

    if not isinstance(ans, int):
        print("The answer should be a Python int but is {}".format(type(ans)))
        return -1


    try:
        v = values[str(i)]
    except KeyError:
        print("answer not available")
        return ERROR

    b = bytes(str(ans), encoding='ascii')
    h = hashlib.sha1(b).hexdigest()

    if h != v:
        print("{} is a wrong answer".format(ans, h, v))
        return WRONG_ANSWER
    elif t > 1:
        print("{} success but slow".format(ans))
        return GOOD_SLOW_ANSWER
    else:
        print("{} success and efficiency".format(ans))
        return GOOD_FAST_ANSWER

if __name__ == '__main__':
    d = {ans: [] for ans in ans_types}

    if len(sys.argv) > 1:
        pbs = [int(i) for i in sys.argv[1:]]
    else:
        pbs = [filename for filename in os.listdir(os.curdir) if re.match(name, filename)]
        pbs = [int(re.sub(name, "\\1", pb)) for pb in pbs]
        pbs.sort()

    for i in pbs:
        filename = "problem{}.py".format(i)
        ans = check_answer(i)
        d[ans].append(i)

    for ans in ans_types:
        d[ans].sort()

    print("*"*30)
    print("{} good fast answers (problems {})".format(len(d[GOOD_FAST_ANSWER]), d[GOOD_FAST_ANSWER]))
    print("{} good slow answers (problems {})".format(len(d[GOOD_SLOW_ANSWER]), d[GOOD_SLOW_ANSWER]))
    print("{} wrong answers (problems {})".format(len(d[WRONG_ANSWER]), d[WRONG_ANSWER]))
    print("{} timeout (problems {})".format(len(d[TIMEOUT]), d[TIMEOUT]))
    print("{} errors (problems {})".format(len(d[ERROR]), d[ERROR]))
    print("*"*30)
